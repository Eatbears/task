import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CountCommonService {

  private count = 0;

  public decrement(count:number) {
    this.count = count;
    return --this.count;
  }

  public increment(count:number) {
    this.count = count;
    return ++this.count;
  }
}
