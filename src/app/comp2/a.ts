import { Component } from '@angular/core';
import { CountCommonService } from '../count-common.service';

@Component({
  selector: 'comp-two',
  templateUrl: '../app.component.html',
  styleUrls: ['../app.component.css']
})

export class compTwo {
  public count = 0;
  private compNumber = 2;
  constructor(private _CountServiceServiceCommon: CountCommonService) {
  }
  
  decrem(count: number) {
    this.count = this._CountServiceServiceCommon.decrement(this.count);
  }
  increm(count: number) {
    this.count = this._CountServiceServiceCommon.increment(this.count);
  }
}
