import { TestBed, inject } from '@angular/core/testing';

import { CountCommonService } from './count-common.service';

describe('CountCommonService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CountCommonService]
    });
  });

  it('should be created', inject([CountCommonService], (service: CountCommonService) => {
    expect(service).toBeTruthy();
  }));
});
