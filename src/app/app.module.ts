import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CountServiceService } from './individual/count-service.service'
import { AppComponent } from './individual/a';
import { compThird } from './comp3/a';
import { compOne } from './comp1/a';
import { compTwo } from './comp2/a'

@NgModule({
  declarations: [
    AppComponent, compThird, compOne, compTwo
  ],
  imports: [
    BrowserModule
  ],
  providers: [CountServiceService],
  bootstrap: [AppComponent, compThird, compOne, compTwo]
})
export class AppModule { }
