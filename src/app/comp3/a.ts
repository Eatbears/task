import { Component } from '@angular/core';
import { CountCommonService } from '../count-common.service';

@Component({
  selector: 'comp-three',
  templateUrl: '../app.component.html',
  styleUrls: ['../app.component.css']
})

export class compThird {
  public count = 0;
  private compNumber = 3;
  constructor(private _CountServiceServiceCommon: CountCommonService) {
  }

  decrem(count: number) {
    this.count = this._CountServiceServiceCommon.decrement(this.count);
  }
  increm(count: number) {
    this.count = this._CountServiceServiceCommon.increment(this.count);
  }
}
