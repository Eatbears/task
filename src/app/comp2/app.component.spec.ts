import { TestBed, async } from '@angular/core/testing';
import { compTwo } from './a';
describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        compTwo
      ],
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(compTwo);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`should have as title 'task'`, async(() => {
    const fixture = TestBed.createComponent(compTwo);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('task');
  }));
  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(compTwo);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Welcome to task!');
  }));
});
