import { Component } from '@angular/core';
import { CountServiceService } from './count-service.service';

@Component({
  selector: 'individual',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  public count;

  constructor(private _CountServiceService: CountServiceService) {
  }
  decrem(count: number) {
    this.count = this._CountServiceService.decrement(count);
  }
  increm(count: number) {
    this.count = this._CountServiceService.increment(count);
  }
  ngOnInit() {
    this.count = this._CountServiceService.getNumber();
  }
}
