import { Component } from '@angular/core';
import { CountCommonService } from '../count-common.service';

@Component({
  selector: 'comp-one',
  templateUrl: '../app.component.html',
  styleUrls: ['../app.component.css']
})

export class compOne {
  public count = 0;
  private compNumber = 1;
  constructor(private _CountServiceServiceCommon: CountCommonService) {
  }
  decrem(count: number) {
    this.count = this._CountServiceServiceCommon.decrement(count);
  }
  increm(count: number) {
    this.count = this._CountServiceServiceCommon.increment(count);
  }
}