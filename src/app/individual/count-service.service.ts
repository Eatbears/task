import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CountServiceService {
  private count = 0;

  public getNumber() {
    return this.count;
  }

  public decrement(count: number) {
    return --this.count;
  }

  public increment(count: number) {
    return ++this.count;
  }
}
